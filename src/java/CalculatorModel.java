
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="CalculatorModel")
@SessionScoped
public class CalculatorModel {
private double firstNumber;
private double secondNumber;
private Double result;
private int operation;
public CalculatorModel() {
}




public int getOperation() {
	return operation;
}




public void setOperation(int operation) {
	this.operation = operation;
}




public double getFirstNumber() {
	return firstNumber;
}




public void setFirstNumber(double firstNumber) {
	this.firstNumber = firstNumber;
}




public double getSecondNumber() {
	return secondNumber;
}




public void setSecondNumber(double secondNumber) {
	this.secondNumber = secondNumber;
}




public Double getResult() {
	return result;
}




public void setResult(Double result) {
	this.result = result;
}




public String reset() {
	this.firstNumber = 0.0;
	this.secondNumber = 0.0;
	this.result = null;
	return "reset";
}
public String performCalculations() {
	try {
	
	switch(operation){
		case 1:{
			result = firstNumber + secondNumber;
			break;
		}
		case 2:{
			result = firstNumber - secondNumber;
			break;
		}
		case 3:{
			result = firstNumber * secondNumber;
			break;
		}
		case 4:{
			result = firstNumber / secondNumber;
			break;
		}
		case 5:{
			result = Math.pow(firstNumber, secondNumber);
			break;
		}
	}
	} catch(Exception e) {}
	if(result != null)
	return "calculationsPerformed";
	else
	return "calculationsFail";
	}
}
