/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author pacio
 */
@FacesValidator("radioValidator")
public class RadioValidator implements Validator {

    /**
     * Creates a new instance of RadioValidator
     */
    public RadioValidator() {
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object radioValue) throws ValidatorException {
        Integer radioIntegerValue = (Integer) radioValue;
        UIInput secondNumberInput = (UIInput) component.findComponent("secondNumber");
        Double secondNumber = (Double) secondNumberInput.getValue();
        if (radioIntegerValue == null || radioIntegerValue < 1 || radioIntegerValue > 4) {
            FacesMessage msg = new FacesMessage("Nieprawidlowa operacja", "Nieprawidlowa operacja "); 
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        } else {
            if (radioIntegerValue == 4 && secondNumber.equals(new Double(0.0))) {
                FacesMessage msg = new FacesMessage("Nie mozna dzielic przez zero", "Nie mozna dzielic przez zero");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }

    }

}
