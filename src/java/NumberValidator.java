/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author pacio
 */

@FacesValidator("numberValidator")
public class NumberValidator implements Validator{

    /**
     * Creates a new instance of NumberValidator
     */
    public NumberValidator() {
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object number) throws ValidatorException {
        Double dnumber = (Double)number; 
        if(dnumber == null || dnumber < 0) { 
            FacesMessage msg = new FacesMessage("Incorrect number", "Nieprawidlowa liczba");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR); 
            throw new ValidatorException(msg); 
        } 
    }
    
}
