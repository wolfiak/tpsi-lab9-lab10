package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class CalculatorView_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_f_validator_validatorId_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_message_style_for_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_outputLabel_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_selectOneRadio_value_id;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_commandButton_value_action_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_inputText_value_id;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_choose;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_panelGrid_columns;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_f_view;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_panelGroup_rendered;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_form;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_when_test;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_h_panelGroup;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_f_validator_validatorId_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_message_style_for_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_outputLabel_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_selectOneRadio_value_id = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_commandButton_value_action_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_inputText_value_id = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_choose = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_panelGrid_columns = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_f_view = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_panelGroup_rendered = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_form = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_when_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_h_panelGroup = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_f_validator_validatorId_nobody.release();
    _jspx_tagPool_h_message_style_for_nobody.release();
    _jspx_tagPool_h_outputLabel_value_nobody.release();
    _jspx_tagPool_h_selectOneRadio_value_id.release();
    _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.release();
    _jspx_tagPool_h_commandButton_value_action_nobody.release();
    _jspx_tagPool_h_inputText_value_id.release();
    _jspx_tagPool_c_choose.release();
    _jspx_tagPool_h_panelGrid_columns.release();
    _jspx_tagPool_f_view.release();
    _jspx_tagPool_h_panelGroup_rendered.release();
    _jspx_tagPool_h_form.release();
    _jspx_tagPool_c_when_test.release();
    _jspx_tagPool_h_panelGroup.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      if (_jspx_meth_f_view_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_f_view_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  f:view
    com.sun.faces.taglib.jsf_core.ViewTag _jspx_th_f_view_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.jsf_core.ViewTag.class) : new com.sun.faces.taglib.jsf_core.ViewTag();
    _jspx_th_f_view_0.setPageContext(_jspx_page_context);
    _jspx_th_f_view_0.setParent(null);
    _jspx_th_f_view_0.setJspId("id14");
    int _jspx_eval_f_view_0 = _jspx_th_f_view_0.doStartTag();
    if (_jspx_eval_f_view_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_f_view_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_f_view_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_f_view_0.doInitBody();
      }
      do {
        out.write("\r\n");
        out.write("-------------------\r\n");
        if (_jspx_meth_h_form_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_f_view_0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_h_panelGroup_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_f_view_0, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("--------------------\r\n");
        int evalDoAfterBody = _jspx_th_f_view_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_f_view_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_f_view_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_f_view.reuse(_jspx_th_f_view_0);
      return true;
    }
    _jspx_tagPool_f_view.reuse(_jspx_th_f_view_0);
    return false;
  }

  private boolean _jspx_meth_h_form_0(javax.servlet.jsp.tagext.JspTag _jspx_th_f_view_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:form
    com.sun.faces.taglib.html_basic.FormTag _jspx_th_h_form_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.FormTag.class) : new com.sun.faces.taglib.html_basic.FormTag();
    _jspx_th_h_form_0.setPageContext(_jspx_page_context);
    _jspx_th_h_form_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_f_view_0);
    _jspx_th_h_form_0.setJspId("id16");
    int _jspx_eval_h_form_0 = _jspx_th_h_form_0.doStartTag();
    if (_jspx_eval_h_form_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_form_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_form_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_form_0.doInitBody();
      }
      do {
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_h_panelGrid_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_form_0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_h_commandButton_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_form_0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_h_commandButton_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_form_0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        int evalDoAfterBody = _jspx_th_h_form_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_form_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_form_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_form.reuse(_jspx_th_h_form_0);
      return true;
    }
    _jspx_tagPool_h_form.reuse(_jspx_th_h_form_0);
    return false;
  }

  private boolean _jspx_meth_h_panelGrid_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_form_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:panelGrid
    com.sun.faces.taglib.html_basic.PanelGridTag _jspx_th_h_panelGrid_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.PanelGridTag.class) : new com.sun.faces.taglib.html_basic.PanelGridTag();
    _jspx_th_h_panelGrid_0.setPageContext(_jspx_page_context);
    _jspx_th_h_panelGrid_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_form_0);
    _jspx_th_h_panelGrid_0.setJspId("id18");
    _jspx_th_h_panelGrid_0.setColumns(org.apache.jasper.runtime.PageContextImpl.getValueExpression("3", (PageContext)_jspx_page_context, int.class, null));
    int _jspx_eval_h_panelGrid_0 = _jspx_th_h_panelGrid_0.doStartTag();
    if (_jspx_eval_h_panelGrid_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_panelGrid_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_panelGrid_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_panelGrid_0.doInitBody();
      }
      do {
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_h_panelGroup_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGrid_0, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("<br/> \r\n");
        if (_jspx_meth_h_message_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGrid_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        if (_jspx_meth_h_panelGroup_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGrid_0, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("<br/> \r\n");
        if (_jspx_meth_h_message_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGrid_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        if (_jspx_meth_h_panelGroup_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGrid_0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        int evalDoAfterBody = _jspx_th_h_panelGrid_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_panelGrid_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_panelGrid_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_panelGrid_columns.reuse(_jspx_th_h_panelGrid_0);
      return true;
    }
    _jspx_tagPool_h_panelGrid_columns.reuse(_jspx_th_h_panelGrid_0);
    return false;
  }

  private boolean _jspx_meth_h_panelGroup_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGrid_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:panelGroup
    com.sun.faces.taglib.html_basic.PanelGroupTag _jspx_th_h_panelGroup_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.PanelGroupTag.class) : new com.sun.faces.taglib.html_basic.PanelGroupTag();
    _jspx_th_h_panelGroup_0.setPageContext(_jspx_page_context);
    _jspx_th_h_panelGroup_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGrid_0);
    _jspx_th_h_panelGroup_0.setJspId("id20");
    int _jspx_eval_h_panelGroup_0 = _jspx_th_h_panelGroup_0.doStartTag();
    if (_jspx_eval_h_panelGroup_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_panelGroup_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_panelGroup_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_panelGroup_0.doInitBody();
      }
      do {
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_h_outputLabel_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_0, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("<br/>\r\n");
        if (_jspx_meth_h_inputText_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        int evalDoAfterBody = _jspx_th_h_panelGroup_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_panelGroup_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_panelGroup_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_panelGroup.reuse(_jspx_th_h_panelGroup_0);
      return true;
    }
    _jspx_tagPool_h_panelGroup.reuse(_jspx_th_h_panelGroup_0);
    return false;
  }

  private boolean _jspx_meth_h_outputLabel_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:outputLabel
    com.sun.faces.taglib.html_basic.OutputLabelTag _jspx_th_h_outputLabel_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.OutputLabelTag.class) : new com.sun.faces.taglib.html_basic.OutputLabelTag();
    _jspx_th_h_outputLabel_0.setPageContext(_jspx_page_context);
    _jspx_th_h_outputLabel_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_0);
    _jspx_th_h_outputLabel_0.setJspId("id22");
    _jspx_th_h_outputLabel_0.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("Pierwszy numerr", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_outputLabel_0 = _jspx_th_h_outputLabel_0.doStartTag();
    if (_jspx_th_h_outputLabel_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_0);
      return true;
    }
    _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_0);
    return false;
  }

  private boolean _jspx_meth_h_inputText_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:inputText
    com.sun.faces.taglib.html_basic.InputTextTag _jspx_th_h_inputText_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.InputTextTag.class) : new com.sun.faces.taglib.html_basic.InputTextTag();
    _jspx_th_h_inputText_0.setPageContext(_jspx_page_context);
    _jspx_th_h_inputText_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_0);
    _jspx_th_h_inputText_0.setJspId("id25");
    _jspx_th_h_inputText_0.setId("firstNumber");
    _jspx_th_h_inputText_0.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("#{CalculatorModel.firstNumber}", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_inputText_0 = _jspx_th_h_inputText_0.doStartTag();
    if (_jspx_eval_h_inputText_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_inputText_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_inputText_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_inputText_0.doInitBody();
      }
      do {
        out.write(" \r\n");
        if (_jspx_meth_f_validator_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_inputText_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        int evalDoAfterBody = _jspx_th_h_inputText_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_inputText_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_inputText_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_inputText_value_id.reuse(_jspx_th_h_inputText_0);
      return true;
    }
    _jspx_tagPool_h_inputText_value_id.reuse(_jspx_th_h_inputText_0);
    return false;
  }

  private boolean _jspx_meth_f_validator_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_inputText_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  f:validator
    com.sun.faces.taglib.jsf_core.ValidatorTag _jspx_th_f_validator_0 = (com.sun.faces.taglib.jsf_core.ValidatorTag) _jspx_tagPool_f_validator_validatorId_nobody.get(com.sun.faces.taglib.jsf_core.ValidatorTag.class);
    _jspx_th_f_validator_0.setPageContext(_jspx_page_context);
    _jspx_th_f_validator_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_inputText_0);
    _jspx_th_f_validator_0.setValidatorId(org.apache.jasper.runtime.PageContextImpl.getValueExpression("numberValidator", (PageContext)_jspx_page_context, java.lang.String.class, null));
    int _jspx_eval_f_validator_0 = _jspx_th_f_validator_0.doStartTag();
    if (_jspx_th_f_validator_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_f_validator_validatorId_nobody.reuse(_jspx_th_f_validator_0);
      return true;
    }
    _jspx_tagPool_f_validator_validatorId_nobody.reuse(_jspx_th_f_validator_0);
    return false;
  }

  private boolean _jspx_meth_h_message_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGrid_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:message
    com.sun.faces.taglib.html_basic.MessageTag _jspx_th_h_message_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.MessageTag.class) : new com.sun.faces.taglib.html_basic.MessageTag();
    _jspx_th_h_message_0.setPageContext(_jspx_page_context);
    _jspx_th_h_message_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGrid_0);
    _jspx_th_h_message_0.setJspId("id32");
    _jspx_th_h_message_0.setFor(org.apache.jasper.runtime.PageContextImpl.getValueExpression("firstNumber", (PageContext)_jspx_page_context, java.lang.String.class, null));
    _jspx_th_h_message_0.setStyle(org.apache.jasper.runtime.PageContextImpl.getValueExpression("color:red; font-size: 11px;", (PageContext)_jspx_page_context, java.lang.String.class, null));
    int _jspx_eval_h_message_0 = _jspx_th_h_message_0.doStartTag();
    if (_jspx_th_h_message_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_message_style_for_nobody.reuse(_jspx_th_h_message_0);
      return true;
    }
    _jspx_tagPool_h_message_style_for_nobody.reuse(_jspx_th_h_message_0);
    return false;
  }

  private boolean _jspx_meth_h_panelGroup_1(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGrid_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:panelGroup
    com.sun.faces.taglib.html_basic.PanelGroupTag _jspx_th_h_panelGroup_1 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.PanelGroupTag.class) : new com.sun.faces.taglib.html_basic.PanelGroupTag();
    _jspx_th_h_panelGroup_1.setPageContext(_jspx_page_context);
    _jspx_th_h_panelGroup_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGrid_0);
    _jspx_th_h_panelGroup_1.setJspId("id34");
    int _jspx_eval_h_panelGroup_1 = _jspx_th_h_panelGroup_1.doStartTag();
    if (_jspx_eval_h_panelGroup_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_panelGroup_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_panelGroup_1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_panelGroup_1.doInitBody();
      }
      do {
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_h_outputLabel_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_1, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("<br/>\r\n");
        if (_jspx_meth_h_inputText_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_1, _jspx_page_context))
          return true;
        out.write(" \r\n");
        int evalDoAfterBody = _jspx_th_h_panelGroup_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_panelGroup_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_panelGroup_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_panelGroup.reuse(_jspx_th_h_panelGroup_1);
      return true;
    }
    _jspx_tagPool_h_panelGroup.reuse(_jspx_th_h_panelGroup_1);
    return false;
  }

  private boolean _jspx_meth_h_outputLabel_1(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:outputLabel
    com.sun.faces.taglib.html_basic.OutputLabelTag _jspx_th_h_outputLabel_1 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.OutputLabelTag.class) : new com.sun.faces.taglib.html_basic.OutputLabelTag();
    _jspx_th_h_outputLabel_1.setPageContext(_jspx_page_context);
    _jspx_th_h_outputLabel_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_1);
    _jspx_th_h_outputLabel_1.setJspId("id36");
    _jspx_th_h_outputLabel_1.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("Drugi numer", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_outputLabel_1 = _jspx_th_h_outputLabel_1.doStartTag();
    if (_jspx_th_h_outputLabel_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_1);
      return true;
    }
    _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_1);
    return false;
  }

  private boolean _jspx_meth_h_inputText_1(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:inputText
    com.sun.faces.taglib.html_basic.InputTextTag _jspx_th_h_inputText_1 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.InputTextTag.class) : new com.sun.faces.taglib.html_basic.InputTextTag();
    _jspx_th_h_inputText_1.setPageContext(_jspx_page_context);
    _jspx_th_h_inputText_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_1);
    _jspx_th_h_inputText_1.setJspId("id39");
    _jspx_th_h_inputText_1.setId("secondNumber");
    _jspx_th_h_inputText_1.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("#{CalculatorModel.secondNumber}", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_inputText_1 = _jspx_th_h_inputText_1.doStartTag();
    if (_jspx_eval_h_inputText_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_inputText_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_inputText_1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_inputText_1.doInitBody();
      }
      do {
        out.write(" \r\n");
        if (_jspx_meth_f_validator_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_inputText_1, _jspx_page_context))
          return true;
        out.write(" \r\n");
        int evalDoAfterBody = _jspx_th_h_inputText_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_inputText_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_inputText_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_inputText_value_id.reuse(_jspx_th_h_inputText_1);
      return true;
    }
    _jspx_tagPool_h_inputText_value_id.reuse(_jspx_th_h_inputText_1);
    return false;
  }

  private boolean _jspx_meth_f_validator_1(javax.servlet.jsp.tagext.JspTag _jspx_th_h_inputText_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  f:validator
    com.sun.faces.taglib.jsf_core.ValidatorTag _jspx_th_f_validator_1 = (com.sun.faces.taglib.jsf_core.ValidatorTag) _jspx_tagPool_f_validator_validatorId_nobody.get(com.sun.faces.taglib.jsf_core.ValidatorTag.class);
    _jspx_th_f_validator_1.setPageContext(_jspx_page_context);
    _jspx_th_f_validator_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_inputText_1);
    _jspx_th_f_validator_1.setValidatorId(org.apache.jasper.runtime.PageContextImpl.getValueExpression("numberValidator", (PageContext)_jspx_page_context, java.lang.String.class, null));
    int _jspx_eval_f_validator_1 = _jspx_th_f_validator_1.doStartTag();
    if (_jspx_th_f_validator_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_f_validator_validatorId_nobody.reuse(_jspx_th_f_validator_1);
      return true;
    }
    _jspx_tagPool_f_validator_validatorId_nobody.reuse(_jspx_th_f_validator_1);
    return false;
  }

  private boolean _jspx_meth_h_message_1(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGrid_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:message
    com.sun.faces.taglib.html_basic.MessageTag _jspx_th_h_message_1 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.MessageTag.class) : new com.sun.faces.taglib.html_basic.MessageTag();
    _jspx_th_h_message_1.setPageContext(_jspx_page_context);
    _jspx_th_h_message_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGrid_0);
    _jspx_th_h_message_1.setJspId("id46");
    _jspx_th_h_message_1.setFor(org.apache.jasper.runtime.PageContextImpl.getValueExpression("secondNumber", (PageContext)_jspx_page_context, java.lang.String.class, null));
    _jspx_th_h_message_1.setStyle(org.apache.jasper.runtime.PageContextImpl.getValueExpression("color:red; font-size: 11px;", (PageContext)_jspx_page_context, java.lang.String.class, null));
    int _jspx_eval_h_message_1 = _jspx_th_h_message_1.doStartTag();
    if (_jspx_th_h_message_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_message_style_for_nobody.reuse(_jspx_th_h_message_1);
      return true;
    }
    _jspx_tagPool_h_message_style_for_nobody.reuse(_jspx_th_h_message_1);
    return false;
  }

  private boolean _jspx_meth_h_panelGroup_2(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGrid_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:panelGroup
    com.sun.faces.taglib.html_basic.PanelGroupTag _jspx_th_h_panelGroup_2 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.PanelGroupTag.class) : new com.sun.faces.taglib.html_basic.PanelGroupTag();
    _jspx_th_h_panelGroup_2.setPageContext(_jspx_page_context);
    _jspx_th_h_panelGroup_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGrid_0);
    _jspx_th_h_panelGroup_2.setJspId("id48");
    int _jspx_eval_h_panelGroup_2 = _jspx_th_h_panelGroup_2.doStartTag();
    if (_jspx_eval_h_panelGroup_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_panelGroup_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_panelGroup_2.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_panelGroup_2.doInitBody();
      }
      do {
        out.write(" \r\n");
        out.write(" \r\n");
        out.write("\t\r\n");
        out.write("\t");
        if (_jspx_meth_h_selectOneRadio_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_2, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("        ");
        if (_jspx_meth_h_message_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_2, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("\r\n");
        int evalDoAfterBody = _jspx_th_h_panelGroup_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_panelGroup_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_panelGroup_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_panelGroup.reuse(_jspx_th_h_panelGroup_2);
      return true;
    }
    _jspx_tagPool_h_panelGroup.reuse(_jspx_th_h_panelGroup_2);
    return false;
  }

  private boolean _jspx_meth_h_selectOneRadio_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:selectOneRadio
    com.sun.faces.taglib.html_basic.SelectOneRadioTag _jspx_th_h_selectOneRadio_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.SelectOneRadioTag.class) : new com.sun.faces.taglib.html_basic.SelectOneRadioTag();
    _jspx_th_h_selectOneRadio_0.setPageContext(_jspx_page_context);
    _jspx_th_h_selectOneRadio_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_2);
    _jspx_th_h_selectOneRadio_0.setJspId("id50");
    _jspx_th_h_selectOneRadio_0.setId("operations");
    _jspx_th_h_selectOneRadio_0.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("#{CalculatorModel.operation}", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_selectOneRadio_0 = _jspx_th_h_selectOneRadio_0.doStartTag();
    if (_jspx_eval_h_selectOneRadio_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_selectOneRadio_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_selectOneRadio_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_selectOneRadio_0.doInitBody();
      }
      do {
        out.write(" \r\n");
        out.write("            ");
        if (_jspx_meth_f_selectItem_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_selectOneRadio_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("            ");
        if (_jspx_meth_f_selectItem_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_selectOneRadio_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("            ");
        if (_jspx_meth_f_selectItem_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_selectOneRadio_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("            ");
        if (_jspx_meth_f_selectItem_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_selectOneRadio_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("            ");
        if (_jspx_meth_f_validator_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_selectOneRadio_0, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("        ");
        int evalDoAfterBody = _jspx_th_h_selectOneRadio_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_selectOneRadio_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_selectOneRadio_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_selectOneRadio_value_id.reuse(_jspx_th_h_selectOneRadio_0);
      return true;
    }
    _jspx_tagPool_h_selectOneRadio_value_id.reuse(_jspx_th_h_selectOneRadio_0);
    return false;
  }

  private boolean _jspx_meth_f_selectItem_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_selectOneRadio_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  f:selectItem
    com.sun.faces.taglib.jsf_core.SelectItemTag _jspx_th_f_selectItem_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.jsf_core.SelectItemTag.class) : new com.sun.faces.taglib.jsf_core.SelectItemTag();
    _jspx_th_f_selectItem_0.setPageContext(_jspx_page_context);
    _jspx_th_f_selectItem_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_selectOneRadio_0);
    _jspx_th_f_selectItem_0.setJspId("id52");
    _jspx_th_f_selectItem_0.setId("add");
    _jspx_th_f_selectItem_0.setItemLabel(org.apache.jasper.runtime.PageContextImpl.getValueExpression("Dodaj", (PageContext)_jspx_page_context, java.lang.String.class, null));
    _jspx_th_f_selectItem_0.setItemValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("1", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_f_selectItem_0 = _jspx_th_f_selectItem_0.doStartTag();
    if (_jspx_th_f_selectItem_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.reuse(_jspx_th_f_selectItem_0);
      return true;
    }
    _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.reuse(_jspx_th_f_selectItem_0);
    return false;
  }

  private boolean _jspx_meth_f_selectItem_1(javax.servlet.jsp.tagext.JspTag _jspx_th_h_selectOneRadio_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  f:selectItem
    com.sun.faces.taglib.jsf_core.SelectItemTag _jspx_th_f_selectItem_1 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.jsf_core.SelectItemTag.class) : new com.sun.faces.taglib.jsf_core.SelectItemTag();
    _jspx_th_f_selectItem_1.setPageContext(_jspx_page_context);
    _jspx_th_f_selectItem_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_selectOneRadio_0);
    _jspx_th_f_selectItem_1.setJspId("id54");
    _jspx_th_f_selectItem_1.setId("subtract");
    _jspx_th_f_selectItem_1.setItemLabel(org.apache.jasper.runtime.PageContextImpl.getValueExpression("Odejmij", (PageContext)_jspx_page_context, java.lang.String.class, null));
    _jspx_th_f_selectItem_1.setItemValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("2", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_f_selectItem_1 = _jspx_th_f_selectItem_1.doStartTag();
    if (_jspx_th_f_selectItem_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.reuse(_jspx_th_f_selectItem_1);
      return true;
    }
    _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.reuse(_jspx_th_f_selectItem_1);
    return false;
  }

  private boolean _jspx_meth_f_selectItem_2(javax.servlet.jsp.tagext.JspTag _jspx_th_h_selectOneRadio_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  f:selectItem
    com.sun.faces.taglib.jsf_core.SelectItemTag _jspx_th_f_selectItem_2 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.jsf_core.SelectItemTag.class) : new com.sun.faces.taglib.jsf_core.SelectItemTag();
    _jspx_th_f_selectItem_2.setPageContext(_jspx_page_context);
    _jspx_th_f_selectItem_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_selectOneRadio_0);
    _jspx_th_f_selectItem_2.setJspId("id56");
    _jspx_th_f_selectItem_2.setId("multiply");
    _jspx_th_f_selectItem_2.setItemLabel(org.apache.jasper.runtime.PageContextImpl.getValueExpression("Mnoz", (PageContext)_jspx_page_context, java.lang.String.class, null));
    _jspx_th_f_selectItem_2.setItemValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("3", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_f_selectItem_2 = _jspx_th_f_selectItem_2.doStartTag();
    if (_jspx_th_f_selectItem_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.reuse(_jspx_th_f_selectItem_2);
      return true;
    }
    _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.reuse(_jspx_th_f_selectItem_2);
    return false;
  }

  private boolean _jspx_meth_f_selectItem_3(javax.servlet.jsp.tagext.JspTag _jspx_th_h_selectOneRadio_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  f:selectItem
    com.sun.faces.taglib.jsf_core.SelectItemTag _jspx_th_f_selectItem_3 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.jsf_core.SelectItemTag.class) : new com.sun.faces.taglib.jsf_core.SelectItemTag();
    _jspx_th_f_selectItem_3.setPageContext(_jspx_page_context);
    _jspx_th_f_selectItem_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_selectOneRadio_0);
    _jspx_th_f_selectItem_3.setJspId("id58");
    _jspx_th_f_selectItem_3.setId("divide");
    _jspx_th_f_selectItem_3.setItemLabel(org.apache.jasper.runtime.PageContextImpl.getValueExpression("Dziel", (PageContext)_jspx_page_context, java.lang.String.class, null));
    _jspx_th_f_selectItem_3.setItemValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("4", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_f_selectItem_3 = _jspx_th_f_selectItem_3.doStartTag();
    if (_jspx_th_f_selectItem_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.reuse(_jspx_th_f_selectItem_3);
      return true;
    }
    _jspx_tagPool_f_selectItem_itemValue_itemLabel_id_nobody.reuse(_jspx_th_f_selectItem_3);
    return false;
  }

  private boolean _jspx_meth_f_validator_2(javax.servlet.jsp.tagext.JspTag _jspx_th_h_selectOneRadio_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  f:validator
    com.sun.faces.taglib.jsf_core.ValidatorTag _jspx_th_f_validator_2 = (com.sun.faces.taglib.jsf_core.ValidatorTag) _jspx_tagPool_f_validator_validatorId_nobody.get(com.sun.faces.taglib.jsf_core.ValidatorTag.class);
    _jspx_th_f_validator_2.setPageContext(_jspx_page_context);
    _jspx_th_f_validator_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_selectOneRadio_0);
    _jspx_th_f_validator_2.setValidatorId(org.apache.jasper.runtime.PageContextImpl.getValueExpression("radioValidator", (PageContext)_jspx_page_context, java.lang.String.class, null));
    int _jspx_eval_f_validator_2 = _jspx_th_f_validator_2.doStartTag();
    if (_jspx_th_f_validator_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_f_validator_validatorId_nobody.reuse(_jspx_th_f_validator_2);
      return true;
    }
    _jspx_tagPool_f_validator_validatorId_nobody.reuse(_jspx_th_f_validator_2);
    return false;
  }

  private boolean _jspx_meth_h_message_2(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:message
    com.sun.faces.taglib.html_basic.MessageTag _jspx_th_h_message_2 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.MessageTag.class) : new com.sun.faces.taglib.html_basic.MessageTag();
    _jspx_th_h_message_2.setPageContext(_jspx_page_context);
    _jspx_th_h_message_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_2);
    _jspx_th_h_message_2.setJspId("id63");
    _jspx_th_h_message_2.setFor(org.apache.jasper.runtime.PageContextImpl.getValueExpression("operations", (PageContext)_jspx_page_context, java.lang.String.class, null));
    _jspx_th_h_message_2.setStyle(org.apache.jasper.runtime.PageContextImpl.getValueExpression("color:red; font-size: 11px;", (PageContext)_jspx_page_context, java.lang.String.class, null));
    int _jspx_eval_h_message_2 = _jspx_th_h_message_2.doStartTag();
    if (_jspx_th_h_message_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_message_style_for_nobody.reuse(_jspx_th_h_message_2);
      return true;
    }
    _jspx_tagPool_h_message_style_for_nobody.reuse(_jspx_th_h_message_2);
    return false;
  }

  private boolean _jspx_meth_h_commandButton_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_form_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:commandButton
    com.sun.faces.taglib.html_basic.CommandButtonTag _jspx_th_h_commandButton_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.CommandButtonTag.class) : new com.sun.faces.taglib.html_basic.CommandButtonTag();
    _jspx_th_h_commandButton_0.setPageContext(_jspx_page_context);
    _jspx_th_h_commandButton_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_form_0);
    _jspx_th_h_commandButton_0.setJspId("id67");
    _jspx_th_h_commandButton_0.setAction(org.apache.jasper.runtime.PageContextImpl.getMethodExpression("#{CalculatorModel.performCalculations}", (PageContext)_jspx_page_context, null, java.lang.Object.class, new Class[] {}));
    _jspx_th_h_commandButton_0.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("Licz", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_commandButton_0 = _jspx_th_h_commandButton_0.doStartTag();
    if (_jspx_th_h_commandButton_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_commandButton_value_action_nobody.reuse(_jspx_th_h_commandButton_0);
      return true;
    }
    _jspx_tagPool_h_commandButton_value_action_nobody.reuse(_jspx_th_h_commandButton_0);
    return false;
  }

  private boolean _jspx_meth_h_commandButton_1(javax.servlet.jsp.tagext.JspTag _jspx_th_h_form_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:commandButton
    com.sun.faces.taglib.html_basic.CommandButtonTag _jspx_th_h_commandButton_1 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.CommandButtonTag.class) : new com.sun.faces.taglib.html_basic.CommandButtonTag();
    _jspx_th_h_commandButton_1.setPageContext(_jspx_page_context);
    _jspx_th_h_commandButton_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_form_0);
    _jspx_th_h_commandButton_1.setJspId("id69");
    _jspx_th_h_commandButton_1.setAction(org.apache.jasper.runtime.PageContextImpl.getMethodExpression("#{CalculatorModel.reset}", (PageContext)_jspx_page_context, null, java.lang.Object.class, new Class[] {}));
    _jspx_th_h_commandButton_1.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("Resetuj", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_commandButton_1 = _jspx_th_h_commandButton_1.doStartTag();
    if (_jspx_th_h_commandButton_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_commandButton_value_action_nobody.reuse(_jspx_th_h_commandButton_1);
      return true;
    }
    _jspx_tagPool_h_commandButton_value_action_nobody.reuse(_jspx_th_h_commandButton_1);
    return false;
  }

  private boolean _jspx_meth_h_panelGroup_3(javax.servlet.jsp.tagext.JspTag _jspx_th_f_view_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:panelGroup
    com.sun.faces.taglib.html_basic.PanelGroupTag _jspx_th_h_panelGroup_3 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.PanelGroupTag.class) : new com.sun.faces.taglib.html_basic.PanelGroupTag();
    _jspx_th_h_panelGroup_3.setPageContext(_jspx_page_context);
    _jspx_th_h_panelGroup_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_f_view_0);
    _jspx_th_h_panelGroup_3.setJspId("id72");
    _jspx_th_h_panelGroup_3.setRendered(org.apache.jasper.runtime.PageContextImpl.getValueExpression("#{CalculatorModel.result != null}", (PageContext)_jspx_page_context, boolean.class, null));
    int _jspx_eval_h_panelGroup_3 = _jspx_th_h_panelGroup_3.doStartTag();
    if (_jspx_eval_h_panelGroup_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_h_panelGroup_3 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_h_panelGroup_3.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_h_panelGroup_3.doInitBody();
      }
      do {
        out.write("\r\n");
        out.write("<h3>Wynik</h3>\r\n");
        if (_jspx_meth_h_outputLabel_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_3, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_c_choose_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_3, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_h_outputLabel_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_3, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("&nbsp;=&nbsp;\r\n");
        if (_jspx_meth_h_outputLabel_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_h_panelGroup_3, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        int evalDoAfterBody = _jspx_th_h_panelGroup_3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_h_panelGroup_3 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_h_panelGroup_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_panelGroup_rendered.reuse(_jspx_th_h_panelGroup_3);
      return true;
    }
    _jspx_tagPool_h_panelGroup_rendered.reuse(_jspx_th_h_panelGroup_3);
    return false;
  }

  private boolean _jspx_meth_h_outputLabel_2(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:outputLabel
    com.sun.faces.taglib.html_basic.OutputLabelTag _jspx_th_h_outputLabel_2 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.OutputLabelTag.class) : new com.sun.faces.taglib.html_basic.OutputLabelTag();
    _jspx_th_h_outputLabel_2.setPageContext(_jspx_page_context);
    _jspx_th_h_outputLabel_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_3);
    _jspx_th_h_outputLabel_2.setJspId("id76");
    _jspx_th_h_outputLabel_2.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("#{CalculatorModel.firstNumber}", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_outputLabel_2 = _jspx_th_h_outputLabel_2.doStartTag();
    if (_jspx_th_h_outputLabel_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_2);
      return true;
    }
    _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_2);
    return false;
  }

  private boolean _jspx_meth_c_choose_0(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:choose
    org.apache.taglibs.standard.tag.common.core.ChooseTag _jspx_th_c_choose_0 = (org.apache.taglibs.standard.tag.common.core.ChooseTag) _jspx_tagPool_c_choose.get(org.apache.taglibs.standard.tag.common.core.ChooseTag.class);
    _jspx_th_c_choose_0.setPageContext(_jspx_page_context);
    _jspx_th_c_choose_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_3);
    int _jspx_eval_c_choose_0 = _jspx_th_c_choose_0.doStartTag();
    if (_jspx_eval_c_choose_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_c_when_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write("\t\r\n");
        if (_jspx_meth_c_when_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write("\t\r\n");
        if (_jspx_meth_c_when_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_c_when_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write("\t\r\n");
        if (_jspx_meth_c_when_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write("\t\r\n");
        out.write("\r\n");
        int evalDoAfterBody = _jspx_th_c_choose_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_choose_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_0);
      return true;
    }
    _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_0);
    return false;
  }

  private boolean _jspx_meth_c_when_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_0 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_0.setPageContext(_jspx_page_context);
    _jspx_th_c_when_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    _jspx_th_c_when_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${CalculatorModel.operation == 1}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_0 = _jspx_th_c_when_0.doStartTag();
    if (_jspx_eval_c_when_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\r\n");
        out.write("&nbsp;+&nbsp;\r\n");
        out.write("\r\n");
        int evalDoAfterBody = _jspx_th_c_when_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_0);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_0);
    return false;
  }

  private boolean _jspx_meth_c_when_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_1 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_1.setPageContext(_jspx_page_context);
    _jspx_th_c_when_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    _jspx_th_c_when_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${CalculatorModel.operation == 2}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_1 = _jspx_th_c_when_1.doStartTag();
    if (_jspx_eval_c_when_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\r\n");
        out.write("&nbsp;-&nbsp;\r\n");
        out.write("\r\n");
        int evalDoAfterBody = _jspx_th_c_when_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_1);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_1);
    return false;
  }

  private boolean _jspx_meth_c_when_2(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_2 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_2.setPageContext(_jspx_page_context);
    _jspx_th_c_when_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    _jspx_th_c_when_2.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${CalculatorModel.operation == 3}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_2 = _jspx_th_c_when_2.doStartTag();
    if (_jspx_eval_c_when_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("&nbsp;*&nbsp;\r\n");
        int evalDoAfterBody = _jspx_th_c_when_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_2);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_2);
    return false;
  }

  private boolean _jspx_meth_c_when_3(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_3 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_3.setPageContext(_jspx_page_context);
    _jspx_th_c_when_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    _jspx_th_c_when_3.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${CalculatorModel.operation == 4}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_3 = _jspx_th_c_when_3.doStartTag();
    if (_jspx_eval_c_when_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("&nbsp;/&nbsp;");
        int evalDoAfterBody = _jspx_th_c_when_3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_3);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_3);
    return false;
  }

  private boolean _jspx_meth_c_when_4(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_4 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_4.setPageContext(_jspx_page_context);
    _jspx_th_c_when_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    _jspx_th_c_when_4.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${CalculatorModel.operation == 5}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_4 = _jspx_th_c_when_4.doStartTag();
    if (_jspx_eval_c_when_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("&nbsp;^&nbsp;");
        int evalDoAfterBody = _jspx_th_c_when_4.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_4);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_4);
    return false;
  }

  private boolean _jspx_meth_h_outputLabel_3(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:outputLabel
    com.sun.faces.taglib.html_basic.OutputLabelTag _jspx_th_h_outputLabel_3 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.OutputLabelTag.class) : new com.sun.faces.taglib.html_basic.OutputLabelTag();
    _jspx_th_h_outputLabel_3.setPageContext(_jspx_page_context);
    _jspx_th_h_outputLabel_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_3);
    _jspx_th_h_outputLabel_3.setJspId("id96");
    _jspx_th_h_outputLabel_3.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("#{CalculatorModel.secondNumber}", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_outputLabel_3 = _jspx_th_h_outputLabel_3.doStartTag();
    if (_jspx_th_h_outputLabel_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_3);
      return true;
    }
    _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_3);
    return false;
  }

  private boolean _jspx_meth_h_outputLabel_4(javax.servlet.jsp.tagext.JspTag _jspx_th_h_panelGroup_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  h:outputLabel
    com.sun.faces.taglib.html_basic.OutputLabelTag _jspx_th_h_outputLabel_4 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(com.sun.faces.taglib.html_basic.OutputLabelTag.class) : new com.sun.faces.taglib.html_basic.OutputLabelTag();
    _jspx_th_h_outputLabel_4.setPageContext(_jspx_page_context);
    _jspx_th_h_outputLabel_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_h_panelGroup_3);
    _jspx_th_h_outputLabel_4.setJspId("id98");
    _jspx_th_h_outputLabel_4.setValue(org.apache.jasper.runtime.PageContextImpl.getValueExpression("#{CalculatorModel.result}", (PageContext)_jspx_page_context, java.lang.Object.class, null));
    int _jspx_eval_h_outputLabel_4 = _jspx_th_h_outputLabel_4.doStartTag();
    if (_jspx_th_h_outputLabel_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_4);
      return true;
    }
    _jspx_tagPool_h_outputLabel_value_nobody.reuse(_jspx_th_h_outputLabel_4);
    return false;
  }
}
