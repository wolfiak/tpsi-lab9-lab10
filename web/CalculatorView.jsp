<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="f"  uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="h"  uri="http://java.sun.com/jsf/html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>


<f:view>
-------------------
<h:form>
<h:panelGrid columns="3">
<h:panelGroup>
<h:outputLabel value="Pierwszy numerr"></h:outputLabel>
<br/>
<h:inputText id="firstNumber" value="#{CalculatorModel.firstNumber}"> 
<f:validator validatorId="numberValidator" /> 
</h:inputText> 
</h:panelGroup>
<br/> 
<h:message for="firstNumber" style="color:red; font-size: 11px;" /> 
<h:panelGroup>
<h:outputLabel value="Drugi numer"></h:outputLabel>
<br/>
<h:inputText id="secondNumber" value="#{CalculatorModel.secondNumber}"> 
<f:validator validatorId="numberValidator" /> 
</h:inputText> 
</h:panelGroup>
<br/> 
<h:message for="secondNumber" style="color:red; font-size: 11px;" /> 
<h:panelGroup> 
 
	
	<h:selectOneRadio id="operations" value="#{CalculatorModel.operation}"> 
            <f:selectItem id="add" itemLabel="Dodaj" itemValue="1" /> 
            <f:selectItem id="subtract" itemLabel="Odejmij" itemValue="2" /> 
            <f:selectItem id="multiply" itemLabel="Mnoz" itemValue="3" /> 
            <f:selectItem id="divide" itemLabel="Dziel" itemValue="4" /> 
            <f:validator validatorId="radioValidator" /> 
        </h:selectOneRadio> 
        <h:message for="operations" style="color:red; font-size: 11px;" /> 

</h:panelGroup>
</h:panelGrid>
<h:commandButton action="#{CalculatorModel.performCalculations}"
value="Licz"></h:commandButton>
<h:commandButton action="#{CalculatorModel.reset}"
value="Resetuj"></h:commandButton>
</h:form>
<h:panelGroup rendered="#{CalculatorModel.result != null}">
<h3>Wynik</h3>
<h:outputLabel
value="#{CalculatorModel.firstNumber}"></h:outputLabel>
<c:choose>
<c:when test="${CalculatorModel.operation == 1}">

&nbsp;+&nbsp;

</c:when>	
<c:when test="${CalculatorModel.operation == 2}">

&nbsp;-&nbsp;

</c:when>	
<c:when test="${CalculatorModel.operation == 3}">
&nbsp;*&nbsp;
</c:when>
<c:when test="${CalculatorModel.operation == 4}">&nbsp;/&nbsp;</c:when>	
<c:when test="${CalculatorModel.operation == 5}">&nbsp;^&nbsp;</c:when>	

</c:choose>
<h:outputLabel
value="#{CalculatorModel.secondNumber}"></h:outputLabel>
&nbsp;=&nbsp;
<h:outputLabel
value="#{CalculatorModel.result}"></h:outputLabel>
</h:panelGroup>
--------------------
</f:view>


</html>